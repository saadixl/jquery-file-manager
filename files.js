var files = [
	{
		"id": 1,
		"name": "folder1",
		"type": "folder",
		"extension": "",
		"path": "/folder1"
	},
	{
		"id": 2,
		"name": "folder2",
		"type": "folder",
		"extension": "",
		"path": "/folder2"
	},
	{
		"id": 3,
		"name": "folder3",
		"type": "folder",
		"extension": "",
		"path": "/folder3"
	},
	{
		"id": 4,
		"name": "file1",
		"type": "file",
		"extension": "txt",
		"path": "/file1.txt"
	},
	{
		"id": 5,
		"name": "file2",
		"type": "file",
		"extension": "txt",
		"path": "/file2.txt"
	},
	{
		"id": 6,
		"name": "pdf1",
		"type": "file",
		"extension": "pdf",
		"path": "/pdf1.pdf"
	},
	{
		"id": 7,
		"name": "folder4",
		"type": "folder",
		"extension": "",
		"path": "/folder4"
	},
	{
		"id": 8,
		"name": "folder5",
		"type": "folder",
		"extension": "",
		"path": "/folder5"
	},
	{
		"id": 9,
		"name": "folder6",
		"type": "folder",
		"extension": "",
		"path": "/folder6"
	},
	{
		"id": 10,
		"name": "file4",
		"type": "file",
		"extension": "txt",
		"path": "/file4.txt"
	},
	{
		"id": 11,
		"name": "file5",
		"type": "file",
		"extension": "txt",
		"path": "/file5.txt"
	},
	{
		"id": 12,
		"name": "pdf2",
		"type": "file",
		"extension": "pdf",
		"path": "/pdf2.pdf"
	}
];
